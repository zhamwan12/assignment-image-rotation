#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "../image/image.h"

struct image rotate_left(struct image src);

#endif
